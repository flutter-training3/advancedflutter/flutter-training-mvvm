import 'package:flutter/foundation.dart';
import 'package:flutter_training_mvvm/models/media.dart';
import 'package:flutter_training_mvvm/models/media_repository.dart';
import 'package:flutter_training_mvvm/models/services/api_response.dart';

class MediaViewModel with ChangeNotifier {
  ApiResponse _apiResponse = ApiResponse.initial('Search the song by Artist');
  Media? _media;

  ApiResponse get response {
    return _apiResponse;
  }

  Media? get media {
    return _media;
  }

  Future<void> featMediaData(String value) async {
    _apiResponse = ApiResponse.loading('Fetch artist data');
    try {
      List<Media> mediaList = await MediaRepository().fetchMediaList(value);
      _apiResponse = ApiResponse.completed(mediaList);
    } catch (e) {
      print("Error : $e");
      _apiResponse = ApiResponse.error('Plase try again!');
    }
    notifyListeners();
  }

  void setSelectedMedia(Media? media) {
    _media = media;
    notifyListeners();
  }
}
