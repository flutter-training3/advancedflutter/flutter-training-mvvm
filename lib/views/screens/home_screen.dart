import 'package:flutter/material.dart';
import 'package:flutter_training_mvvm/models/media.dart';
import 'package:flutter_training_mvvm/models/services/api_response.dart';
import 'package:flutter_training_mvvm/themes/colors.dart';
import 'package:flutter_training_mvvm/view_models/media_view_model.dart';
import 'package:flutter_training_mvvm/views/screens/player_list_widget.dart';
import 'package:flutter_training_mvvm/views/screens/player_widget.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    // กำหนดตัวแปรไว้รับค่าจาก Text field
    final inputController = TextEditingController();
    ApiResponse apiResponse = Provider.of<MediaViewModel>(context).response;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 20),
        child: Column(children: [
          Row(
            children: [
              Expanded(
                  child: Container(
                decoration: BoxDecoration(
                    color: accent, borderRadius: BorderRadius.circular(20)),
                child: TextField(
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        prefixIcon: Icon(
                          Icons.search,
                          color: textIcons,
                        ),
                        hintText: 'Enter Artist Name'),
                    controller: inputController,
                    onChanged: (value) => '',
                    onSubmitted: (value) {
                      Provider.of<MediaViewModel>(context, listen: false)
                          .setSelectedMedia(null);
                      Provider.of<MediaViewModel>(context, listen: false)
                          .featMediaData(value);
                    },
                    style: const TextStyle(fontSize: 16, color: textIcons)),
              ))
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Expanded(child: getMediaWidget(context, apiResponse))
        ]),
      ),
    );
  }

  Widget getMediaWidget(BuildContext context, ApiResponse apiResponse) {
    List<Media>? mediaList = apiResponse.data;
    switch (apiResponse.status) {
      case Status.LOADING:
        return const Center(
          child: CircularProgressIndicator(),
        );
      case Status.COMPLETED:
        return Column(
          children: [
            Expanded(
                flex: 8,
                child: PlayerListWidget(
                    mediaList: mediaList!,
                    function: (Media media) =>
                        Provider.of<MediaViewModel>(context, listen: false)
                            .setSelectedMedia(media))),
            Expanded(
                flex: 2,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: PlayerWidget(function: () {
                    setState(() {});
                  }),
                ))
          ],
        );
      case Status.ERROR:
        return Center(
          child: Text(apiResponse.message!),
        );
      case Status.INITIAL:
      default:
        return Center(
          child: Text(apiResponse.message!),
        );
    }
  }
}
