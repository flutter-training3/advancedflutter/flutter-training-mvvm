import 'package:flutter/material.dart';
import 'package:flutter_training_mvvm/views/screens/home_screen.dart';

final Map<String, WidgetBuilder> routes = {
  '/home': (context) => const HomeScreen()
};
