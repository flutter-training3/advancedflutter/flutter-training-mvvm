class Media {
  String? artistName;
  String? collectionName;
  String? trackName;
  String? artistViewUrl;
  String? previewUrl;
  String? artworkUrl100;

  Media(
      {this.artistName,
      this.collectionName,
      this.trackName,
      this.artistViewUrl,
      this.previewUrl,
      this.artworkUrl100});

  factory Media.fromJson(Map<String, dynamic> json) => Media(
        artistName: json['artistName'] as String?,
        collectionName: json['collectionName'] as String?,
        trackName: json['trackName'] as String?,
        artistViewUrl: json['artistViewUrl'] as String?,
        previewUrl: json['previewUrl'] as String?,
        artworkUrl100: json['artworkUrl100'] as String?,
      );
}
