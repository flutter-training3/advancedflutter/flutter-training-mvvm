import 'package:flutter_training_mvvm/utils/constant.dart';

abstract class BaseService {
  String mediaBaseUrl = baseURLAPI;

  Future<dynamic> getResponse(String url);
}
