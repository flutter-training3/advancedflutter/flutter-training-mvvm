import 'dart:convert';
import 'dart:io';

import 'package:flutter_training_mvvm/models/services/app_exception.dart';
import 'package:flutter_training_mvvm/models/services/base_service.dart';
import 'package:dio/dio.dart';

class MediaService extends BaseService {
  late Dio _dio;
  MediaService() {
    BaseOptions options = BaseOptions(
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    _dio = Dio(options);
  }
  @override
  Future getResponse(String url) async {
    dynamic responseJson;
    try {
      final responses = await _dio.get(super.mediaBaseUrl + url);
      responseJson = returnResponse(responses);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  dynamic returnResponse(Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = json.decode(response.data);
        return responseJson;
      case 400:
        throw BadRequestException(response.data.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.data.toString());
      default:
        throw FetchDataException(
            "Error occured while communication with server ${response.statusCode}");
    }
  }
}
