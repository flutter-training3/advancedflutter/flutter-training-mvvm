import 'package:flutter/material.dart';
import 'package:flutter_training_mvvm/themes/colors.dart';

ThemeData appTheme() {
  return ThemeData(
      fontFamily: 'ChakraPetch',
      primaryColor: primary,
      errorColor: error,
      hoverColor: divider,
      colorScheme: const ColorScheme.light(primary: primary),
      iconTheme: const IconThemeData(color: primaryText),
      scaffoldBackgroundColor: textIcons,
      appBarTheme: const AppBarTheme(
          backgroundColor: primary,
          foregroundColor: textIcons,
          iconTheme: IconThemeData(color: textIcons)));
}
