import 'package:flutter/material.dart';

const primary = Color(0xFFFF5722);
const primaryDark = Color(0xFFE64A19);
const primaryLight = Color(0xFFFFCCBC);
const accent = Color(0xFFE040FB);
const primaryText = Color(0xFF212121);
const secondaryText = Color(0xFF757575);
const textIcons = Color(0xFFFFFFFF);
const divider = Color(0xFFBDBDBD);
const error = Color(0xFFD32F2F);
