import 'package:flutter/material.dart';
import 'package:flutter_training_mvvm/routers.dart';
import 'package:flutter_training_mvvm/themes/style.dart';
import 'package:flutter_training_mvvm/view_models/media_view_model.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: MediaViewModel())],
      child: MaterialApp(
        title: 'Flutter MVVM',
        debugShowCheckedModeBanner: false,
        theme: appTheme(),
        initialRoute: '/home',
        routes: routes,
      ),
    );
    // return MaterialApp(
    //   title: 'Flutter MVVM',
    //   debugShowCheckedModeBanner: false,
    //   theme: appTheme(),
    //   initialRoute: '/home',
    //   routes: routes,
    // );
  }
}
